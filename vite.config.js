import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
const path = require('path')

// https://vitejs.dev/config/

export default defineConfig({
  plugins: [vue()],
  mode: 'build',
  define: {
    'process.env': process.env.NODE_ENV ? { ...process.env, baseURL: 'http://localhost:8081/' } : { ...process.env, baseURL: 'http://production:8081/' }
  },
  optimizeDeps: {
    keepNames: true
  },
  /* build: {
    assetsDir: './src/assets/'
  }, */
  resolve: {
    alias: {
      '@': path.resolve('src')
    }
  }

})
