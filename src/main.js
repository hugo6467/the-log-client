import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import store from '@/store'
import staticConfig from '@/staticConfig'

const app = createApp(App)
app.use(router)
app.use(ElementPlus)
app.provide('tf2Classes', staticConfig.tf2Classes)
app.provide('tf2Weapons', staticConfig.tf2Weapons)
app.provide('weaponStatistics', staticConfig.weaponStatistics)

const getAppConfiguration = () => {
  try {
    axios.get(`http://${window.location.host}/config.json`).then(result => {
      app.provide('configuration', result)
      app.provide('store', store)
      app.mount('#app')
    })
  } catch (error) {
    axios.get('@/../public/config.json').then(result => {
      app.provide('configuration', result)
      app.provide('store', store)
      app.mount('#app')
    })
  }
}
getAppConfiguration()
