export default {
  tf2Classes: {
    scout: 'Scout',
    soldier: 'Soldier',
    pyro: 'Pyro',
    demoman: 'Demoman',
    heavy: 'Heavy',
    engineer: 'Engineer',
    medic: 'Medic',
    sniper: 'Sniper',
    spy: 'Spy'
  },
  weaponStatistics: {
    totalDamage: 'Dano total',
    totalKills: 'Total de kills',
    hits: 'Acertos',
    shots: 'Tiros disparados',
    avg_dmg: 'Dano médio'
  },
  tf2Weapons: {
    panic_attack: 'Panic Attack',
    scattergun: 'Scattergun',
    tf_weapon_handgun_scout_primary: 'Shortstop',
    tf_weapon_soda_popper: 'Soda Popper',
    tf_weapon_pep_brawler_blaster: 'Baby Faces Blaster',
    tf_weapon_pistol_scout: 'Pistol/Lugermorph/C.A.P.P.E.R',
    tf_weapon_handgun_scout_secondary: 'Winger/Pretty Boys Pocket Pistol',
    tf_weapon_cleaver: 'Flying Guillotine',
    tf_weapon_lunchbox_drink: 'Bonk! Atomic Punch/Crit-a-Cola',
    tf_weapon_jar_milk: 'Mad Milk/Mutated Milk',
    tf_weapon_bat: 'Bar/Candy Cane/Batsaber/Boston Basher/Three-Rune Blade/Sun-on-a-Stick/Fan OWar/The Atomizer',
    tf_projectile_bat_wood: 'Sandman',
    tf_weapon_bat_fish: 'Holy Mackerel/Unarmed Combat',
    tf_weapon_bat_giftwrap: 'The Wrap Assassin',
    tf_projectile_rocket: 'Rocket Launcher',
    rocketlauncher_directhit: 'The Direct Hit',
    tf_weapon_particle_cannon: 'The Cow Mangler 5000',
    tf_weapon_rocketlauncher_airstrike: 'The Air Strike',
    tf_weapon_shotgun_soldier: 'Shotgun/The Reserve Shooter',
    tf_wearable: 'Gunboats/The Mantreads',
    tf_weapon_buff_item: 'The Buff Banner/The Battalions Backup/Concheror',
    tf_weapon_raygun: 'The Righteous Bison',
    tf_weapon_parachute: 'The B.A.S.E. Jumper',
    tf_weapon_shovel: 'Shovel/The Equalizer/The Pain Train/Frying Pan/The Market Gardener/The Escape Plan/The Disciplinary Action',
    tf_weapon_katana: 'The Half-Zatoichi'
  }
}
