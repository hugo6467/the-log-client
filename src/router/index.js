import { createRouter, createWebHistory } from 'vue-router'
const Home = () => import('@/App.vue')
const Dashboard = () => import('@/components/View/Dashboard.vue')
const PlayerDetails = () => import('@/components/View/PlayerDetails.vue')
const routerHistory = createWebHistory()

const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/dashboard',
      component: Dashboard
    },
    {
      name: 'player',
      path: '/player/:steamID',
      component: PlayerDetails,
      props: true
    }
  ]
})
export default router
