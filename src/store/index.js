/**
 * Store da aplicação.
 * Uso = import store '@/store'
 * store.methods.set(loggedUser, XPTO)
 */

import { reactive, readonly } from 'vue'

const state = reactive({
  loggedUser: null
})

const methods = {
  set (stateName, value) {
    state[stateName] = value
  }
}
export default {
  state: readonly(state),
  methods
}
